var express = require('express');
var router = express.Router();
let User = require('../model/users');
let Message = require('../model/messages');
const Vonage = require('@vonage/server-sdk');
const from = 'Vonage APIs';
const to = '917895260262';

const vonage = new Vonage({
  apiKey: '848dd946',
  apiSecret: 'yVdkKyVqh6YiIUx8',
});

router.get('/messages', (req, res, next) => {
  Message.find({})
    .populate('userId')
    .exec((error, messages) => {
      if (error) {
        next(error);
      } else {
        res.render('allMessages', { messages: messages });
      }
    });
});

router.get('/:id', (req, res, next) => {
  User.findById(req.params.id, (error, user) => {
    if (error) {
      next(error);
    } else {
      res.render('message', { user: user });
    }
  });
});

router.post('/:id', (req, res, next) => {
  req.body.userId = req.params.id;
  vonage.message.sendSms(from, to, req.body.message, (err, responseData) => {
    if (err) {
      next(err);
    } else {
      if (responseData.messages[0]['status'] === '0') {
        Message.create(req.body, (error, message) => {
          if (error) {
            next(error);
          } else {
            res.render('success');
          }
        });
      } else {
        console.log(
          `Message failed with error: ${responseData.messages[0]['error-text']}`
        );
      }
    }
  });
});

module.exports = router;
