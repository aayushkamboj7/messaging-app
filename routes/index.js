var express = require('express');
var router = express.Router();
let User = require('../model/users');

/* GET home page. */
router.get('/', function (req, res, next) {
  User.find({}, (error, user) => {
    if (error) {
      next(error);
    } else {
      res.render('index', { user: user });
    }
  });
});

module.exports = router;
