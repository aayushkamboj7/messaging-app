let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let messageSchema = new mongoose.Schema({
  message: String,
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
});

let Message = mongoose.model('message', messageSchema);

module.exports = Message;
